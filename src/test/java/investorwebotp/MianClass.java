package investorwebotp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import capability.Capability;
import steps.Login;
import steps.Metabase;

public class MianClass extends Capability{
	WebDriver driver;
	@BeforeTest
	public void openchrome()
	{
		driver = WebCapability();
		driver.manage().window().maximize();
	}
	
	
	

	@Test(priority = 1)
	public void login()
	{
		new Login(driver);
	}
	@Test(priority = 2)
	public void metabase() throws InterruptedException
	{
		new Metabase(driver);
	}
	@AfterTest
	public void CloseBrowser()
	{

		//driver.quit	();
	}

}
